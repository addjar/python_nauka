# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <markdowncell>

# heapq - nlargest() i nsmallest()

# <codecell>

from heapq import nlargest, nsmallest

nums = [1, 4, -3, 12, -3, 5, 45, 100, -90]
print(nlargest(3, nums)) # Wyświetla 3 największe wartości

# <codecell>

print(nsmallest(3, nums)) # Wyświetla 3 najminejsze wartości

# <markdowncell>

# Funkcje z kluczem

# <codecell>

portfolio = [{'name': 'IBM', 'shares': 100, 'price':91.1},
             {'name': 'AAPL', 'shares': 50, 'price':543.22},
             {'name': 'FB', 'shares': 200, 'price':21.09},
             {'name': 'HPQ', 'shares': 35, 'price':31.75},
             {'name': 'YHOO', 'shares': 45, 'price':16.35},
             {'name': 'ACME', 'shares': 75, 'price':115.65}]

cheap = nsmallest(3, portfolio, key=lambda s: s['price'])
for cheap_dict in cheap:
    print(cheap_dict['price'], end=' ')

# <codecell>

expensive = nlargest(3, portfolio, key=lambda s: s['price'])
for expensive_dict in expensive:
    print(expensive_dict['price'], end=' ')

# <markdowncell>

# heapq.heapify()

# <codecell>

from heapq import heapify, heappop
nums = [1, 4, -3, 12, -3, 5, 45, 100, -90]
heap = nums
heapify(heap)
print(heap)

# <rawcell>

# pozycja heap[0] zawsze będzie zawierać najminiejszy element, kolejne elementy można znaleźć za pomocą metody
# heapq.heapop()

# <codecell>

len_heap = len(heap)
for i in range(len_heap):
    print(heappop(heap))

