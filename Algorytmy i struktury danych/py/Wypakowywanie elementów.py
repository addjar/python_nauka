# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <rawcell>

# Wypakowywanie elementów z obiektów interowalnych o dowolnej długości

# <codecell>

record = ('Juzek', 'cos@jakos.pl', '754-321-456', '675-433-566')
name, email, *fone_numbers = record
print(name, end=' ')
print(email, end=' ')
print(fone_numbers)

# <rawcell>

# Wypakowanie za pomocą * jest przydatne w połaczeniu z niektórymi operacjami na łańcuchach znaków np. przy ich dzieleniu

# <codecell>

records = [('foo', 1, 2), 
           ('bar', 'hello'),
           ('foo', 3, 4)]

def do_foo(x, y):
    print('foo', x, y)
    
def do_bar(s):
    print('bar', s)
    
for tag, *args in records:
    if tag == 'foo':
        do_foo(*args)
    elif tag == 'bar':
        do_bar(*args)

# <rawcell>

# zmienna z gwiazdką:

# <codecell>

for tag, *args in records:
    print(*args)

# <rawcell>

# zmienna bez gwiazdki:

# <codecell>

for tag, *args in records:
    print(args)

