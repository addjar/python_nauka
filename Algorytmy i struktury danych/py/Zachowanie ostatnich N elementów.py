# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <rawcell>

# Instrukcja yield

# <codecell>

from collections import  deque
q = deque(maxlen=3)
q.append(1)
q.append(2)
q.append(3)
q

# <codecell>

q.append(4)
q

# <rawcell>

# Instrukcja deque(maxlen=N) tworzy kolejkę o stałej długości. Jeśli program doda nowy element do pełnej kolejkim, automatycznie zostanie usunięty najstarszy element.
# Bez określenia maksymalnego rozmiaru kolejki, otrzymujemy nieograniczoną kolejkę

# <codecell>

q.appendleft(5)
q

# <codecell>

q.pop()

# <codecell>

q

# <codecell>

q.popleft()

# <codecell>

q

# <rawcell>

# Dodawanie i usuwanie obiektów z kolejki ma złożoność O(1)
# Lista = O(N)

