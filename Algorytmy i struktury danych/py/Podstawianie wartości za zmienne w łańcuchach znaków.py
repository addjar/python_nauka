# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

s = '{name} otrzymał {n} wiadomości'
s.format(name='Jan', n=25)

# <rawcell>

# Wykorzystanie wartości ze zmiennych

# <codecell>

s = '{name} ma lat {year}'
name = 'Jacek'
year = 23
s.format_map(vars())

# <rawcell>

# Metoda vars() działa równierz dla obiektów

# <codecell>

s = '{name} ma lat {year}'
class Info:
    def __init__(self, name, year):
        self.name = name
        self.year = year
        
a = Info('Marycha', 55)
s.format_map(vars(a))

