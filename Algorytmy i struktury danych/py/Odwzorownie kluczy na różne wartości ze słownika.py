# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <markdowncell>

# defaultdict

# <rawcell>

# Aby łatwo utworzyć słownik którego wartości będą listą - pozwalają one zachować elementy w kolejności ich wstawiania
# lub zbiory - gdy chce się usunąć powtarzające się elementy należy zastosować obiekt typu:
# defaultdict z modułu collections

# <codecell>

from collections import defaultdict

d = defaultdict(list)
d['a'].append(1)
d['b'].append(2)
d['c'].append(4)
d['a'].append(3)
d

# <codecell>

d = defaultdict(set)
d['a'].add(1)
d['a'].add(2)
d['b'].add(4)
d

# <rawcell>

# Obiekt typu defaultdict automatycznie tworzy w słowniku wpisy dla szukanych później kluczy - nawet gdy klucze pierwotnie nie występują w słowniku
# Utworzenie wielosłownika jest proste, lecz samodzielne inicjowanie pierwszej wartości morze okazać się skąplikowane:

# <codecell>

pairs = {'klucz1': 1, 'klucz2': 4}
d = {}
for key, values in pairs.items():
    if key not in d:
        d[key] = []
    d[key].append(values)
d

# <codecell>

pairs = {'klucz1': 4, 'klucz2': 7}
d = defaultdict(list)
for key, values in pairs.items():
    d[key].append(values)
d

