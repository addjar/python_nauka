# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <rawcell>

# najszybszy:
# 
# O(1) 
#     stała złożoność – algorytm wykonuje się w stałej ilości operacji, niezależnie od liczby danych wejściowych.

# <rawcell>

# bardzo czybki:
# 
# O(log2(n))
# 	złożoność logarytmiczna – algorytm wykonuje logarytmiczną ilość operacji w stosunku do liczby danych wejściowych.
# 
#     * n jest to liczba danych wejściowych;
#     * podstawa logarytmu zazwyczaj wynosi 2, jednak czasami może być większa (np. w B-drzewach).

# <rawcell>

# szybki:
# 
# O(n)
# 	złożoność liniowa – algorytm wykonuje wprost proporcjonalną ilość operacji do liczby danych wejściowych.
# 
#     * n jest to liczba danych wejściowych.

# <rawcell>

# szybki:
# 
# O(n*log2(n))
#     złożoność liniowo-logarytmiczna
# 
#     * n jest to liczba danych wejściowych.

# <rawcell>

# niezbyt szybki:
# 
# O(n2)
#             złożoność kwadratowa – ilość operacji algorytmu jest wprost proporcjonalna do liczby danych wejściowych podniesionej do potęgi drugiej.
# 
#     * n jest to liczba danych wejściowych.

# <rawcell>

# wolny:
# 
# O(nX)
# 
# złożoność wielomianowa – ilość operacji algorytmu jest wprost proporcjonalna do liczby danych wejściowych podniesionej do potęgi X.
# 
#     * n jest to liczba danych wejściowych;
#     * X jest stałą o dowolnej wartości. 

# <rawcell>

# bardzo wolny:
# 
# O(Xn)
# 
# złożoność wykładnicza – ilość operacji algorytmu jest wprost proporcjonalna do stałej X większej lub równej 2, podniesionej do potęgi równej liczbie danych wejściowych.
# 
#     * n jest to liczba danych wejściowych;
#     * X jest stałą większą niż 2.

