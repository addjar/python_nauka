# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

def gen1(n):
    i = 0
    while i < n:
        yield i
        i += 1
       
for i in gen1(10):
    print(i, end='')

# <codecell>

def gen2(n):
    i = 0
    while i < n:
        i += 1
        yield i
       
for i in gen2(10):
    print(i, end='')

# <codecell>

def gen3(napis):
    li = list(napis)
    for ch in li:
        yield ch

for ch in gen3("Ala ma niezłego kota"):
    print(ch, end='')

