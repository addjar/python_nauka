# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <rawcell>

# Do kontrolowania kolejności elementów w słiwniku można wykorzystać obiekt typu OrdereDict z modułu collections

# <codecell>

d1 = {}
d1['a'] = 1
d1['b'] = 2
d1['c'] = 3
d1['d'] = 4
for key in d1:
    print('{0}:{1}'.format(key, d1[key]), end=' ')

# <codecell>

from collections import OrderedDict
d2 = OrderedDict()
d2['a'] = 1
d2['b'] = 2
d2['c'] = 3
d2['d'] = 4
for key in d2:
    print('{0}:{1}'.format(key, d1[key]), end=' ')

# <codecell>

d2['d'] = 5
d2

